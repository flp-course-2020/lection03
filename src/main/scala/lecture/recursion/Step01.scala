package lecture.recursion

object Step01 {
  def square(x: Int) = x * x
  def sumOfSquares(x: Int, y: Int) = square(x) + square(y)
  def f(a: Int) = sumOfSquares(a + 1, a * 2)

  f(5)
  //==>
  sumOfSquares(5 + 1, 5 * 2)
  //==>
  square(6) + square(10)
  //==>
  (6 * 6) + (10 * 10)
  //==>
  136

}
