package lecture.recursion

object Step05 extends App {
  // multiple exits (primitive cases)

  def fibRec(n: Int): Int = {
    def loop(x0: Int, x1: Int, c: Int): Int =
      if (c == 0) x0
      else if (c == 1) x1
      else loop(x1, x0 + x1, c - 1)

    loop(0, 1, n)
  }

  def fibWhile(n: Int, m: Int): Int = {
    var x0 = 0
    var x1 = 1
    var c = n

    while (c > 1) {
      val x2 = x0 + x1
      x0 = x1
      x1 = x2
      c = c - 1
    }
    if (c == 0) x0
    else x1
  }


}
