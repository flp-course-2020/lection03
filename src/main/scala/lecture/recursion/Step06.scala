package lecture.recursion

object Step06 extends App {
  // iteration over structures

  // linked structure
  def sum(list: List[Int]): Int = {
    def loop(acc: Int, l: List[Int]): Int = l match {
      case Nil => acc
      case x :: xs => loop(acc + x, xs)
    }

    loop(1, list)
  }

}
